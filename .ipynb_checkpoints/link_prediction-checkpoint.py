from TADW import TADW

import pandas as pd
import numpy as np
import scipy as sp
import networkx as nx

import time
import random
from random import sample

from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer
from sklearn.metrics import roc_auc_score
from sklearn.metrics.pairwise import cosine_similarity

from nltk.tokenize import RegexpTokenizer


def read_wikipedia(folder_path, ratio_remove):
    # features
    content = pd.read_csv(f"{folder_path}features.txt", sep="\t", header=None, quoting=3)
    vectorizer = TfidfVectorizer(lowercase=True, analyzer="word", stop_words="english", max_df=0.25, min_df=4, norm='l2', use_idf=True)
    features = vectorizer.fit_transform(content[1].values.astype('U'))

    vectorizerTF = TfidfVectorizer(lowercase=True, analyzer="word", stop_words="english", max_df=0.25, min_df=4, norm=None, use_idf=False)
    tf = vectorizerTF.fit_transform(content[1].values.astype('U'))
    tokenizer = RegexpTokenizer(r'\w+')
    raw = [tokenizer.tokenize(i.lower()) for i in content[1].values.astype('U')]
    en_stop = vectorizer.get_stop_words()
    for i in range(len(raw)):
        raw[i] = [word for word in raw[i] if not word in en_stop]
    voc = vectorizer.get_feature_names()

    # graph
    graph = nx.read_adjlist(f"{folder_path}graph.txt", nodetype=int)
    k_remove = int(ratio_remove*len(graph.edges))
    removed_nodes = sample(graph.edges, k_remove)
    train_graph = graph.copy()
    train_graph.remove_edges_from(removed_nodes)
    A = nx.to_scipy_sparse_matrix(train_graph, nodelist=range(features.shape[0]), format="csr")
    return features, A, train_graph, voc, raw, tf, removed_nodes, graph

def make_link_prediction(embeddings, removed_link, true_graph, iterations):
    n_doc = true_graph.number_of_nodes()
    doc_scores = []
    doc_true = []
    for source, target in removed_link:
        sim = cosine_similarity(embeddings[source].reshape(1, -1), embeddings[target].reshape(1, -1))[0,0]
        doc_scores.append(sim)
        doc_true.append(1)
        unlinked_nodes = list(set(range(0,n_doc)) - set([n for n in true_graph.neighbors(source)]))
        for _ in range(0, iterations):
            random_target = random.choice(unlinked_nodes)
            sim = cosine_similarity(embeddings[source].reshape(1, -1), embeddings[random_target].reshape(1, -1))[0,0]
            doc_scores.append(sim)
            doc_true.append(0)
    return roc_auc_score(np.array(doc_true), np.array(doc_scores))

